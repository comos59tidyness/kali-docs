---
title: NetHunter Nmap Scan
description:
icon:
date: 2019-11-29
type: post
weight: 310
author: ["re4son",]
tags: ["",]
keywords: ["",]
og_description:
---

The Nmap Scan pane gives you easy access to the most commonly-used options of the immensely powerful [Nmap](https://nmap.org/) scanner, allowing you to easily launch in-depth scans on targets or networks, without having to type a long string on the command-line with an on-screen keyboard.

![](./nethunter-nmap.png)
